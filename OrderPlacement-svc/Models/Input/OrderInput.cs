﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Moving_svc.Models.Input
{
    public class OrderInput
    {
        public CustomerInput customerInput { get; set; }
        public int? orderStatusId { get; set; }
        [Required(AllowEmptyStrings = false)]
        public int movingFromId { get; set; }
        public string movingFromLocationDetails { get; set; }
        [Required(AllowEmptyStrings = false)]
        public int movingToId { get; set; }
        public string movingToLocationDetails { get; set; }
        [Required(AllowEmptyStrings = false)]
        public List<int> services { get; set; }
        [Required(AllowEmptyStrings = false)]
        public DateTime carriedOut { get; set; }
        public string notes { get; set; }
        [Required(AllowEmptyStrings = false)]
        public int salesConsultantId { get; set; }
    }
}
