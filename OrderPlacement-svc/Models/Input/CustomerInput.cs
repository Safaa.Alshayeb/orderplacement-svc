﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Moving_svc.Models.Input
{
    public class CustomerInput
    {
        [Required(AllowEmptyStrings = false)]
        public string fullName { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string phoneNumber { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string email { get; set; }
    }
}
