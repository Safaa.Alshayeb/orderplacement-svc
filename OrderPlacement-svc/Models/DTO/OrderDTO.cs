﻿
using Moving_svc.Models.OrderPlacementDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moving_svc.Models.DTO
{
    public class OrderDTO
    {
        public int id { get; set; }
        public bool isActivated { get; set; } = true;
        public DateTime creationDate { get; set; }
        public DateTime updatedDate { get; set; }
        public UserDTO customer { get; set; }
        public OrderStatusDTO orderStatus { get; set; }
        public  AddressDTO movingFrom { get; set; }
        public string movingFromLocationDetails { get; set; }
        public AddressDTO movingTo { get; set; }
        public string movingToLocationDetails { get; set; }
        public List<ServiceDTO> services { get; set; }
        public DateTime carriedOut { get; set; }
        public string notes { get; set; }
        public UserDTO salesConsultant { get; set; }
        public OrderDTO(Order order)
        {
            this.id = order.id;
            this.isActivated = order.isActivated;
            this.creationDate = order.creationDate;
            this.updatedDate = order.updatedDate;
            this.notes = order.notes;
            this.movingFrom = new AddressDTO(order.movingFrom);
            this.movingFromLocationDetails = order.movingFromLocationDetails;
            this.movingTo = new AddressDTO(order.movingTo);
            this.movingToLocationDetails = order.movingToLocationDetails;
            this.customer = new UserDTO(order.customer);
            this.carriedOut = order.carriedOut;
            this.salesConsultant = new UserDTO(order.salesConsultant);
            this.services = new List<ServiceDTO>();
            foreach (Order_Service os in order.services)
                {
                    this.services.Add(new ServiceDTO(os.service));
                }
            this.orderStatus = new OrderStatusDTO(order.orderStatus);
        }
    }
}
