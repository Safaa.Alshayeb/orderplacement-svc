﻿using Moving_svc.Models.OrderPlacementDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moving_svc.Models.DTO
{
    public class UserDTO
    {
        public int id { get; set; }
        public string fullName { get; set; }
        public string fatherName { get; set; }
        public string motherName { get; set; }
        public DateTime? birthDate { get; set; }
        public string phoneNumber { get; set; }
        public bool isActivated { get; set; } = true;
        public DateTime creationDate { get; set; }
        public DateTime updatedDate { get; set; }
        public string email { get; set; }
        public UserTypeDTO userType { get; set; }
        public UserDTO(User user)
        {
            this.id = user.id;
            this.fullName = user.fullName;
            this.phoneNumber = user.phoneNumber;
            this.creationDate = user.creationDate;
            this.updatedDate = user.updatedDate;
            this.email = user.email;
            this.isActivated = user.isActivated;
            this.userType = new UserTypeDTO(user.userType);
        }
    }
}
