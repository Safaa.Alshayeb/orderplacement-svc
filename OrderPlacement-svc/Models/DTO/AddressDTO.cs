﻿using Moving_svc.Models.OrderPlacementDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moving_svc.Models.DTO
{
    public class AddressDTO
    {
        public AddressDTO(Area area)
        {
            this.id = area.id;
            this.areaName = area.name;
            this.cityName = area.city.name;
            this.governorateName = area.city.governorate.name;
        }
        public int id { get; set; }
        public string areaName { get; set; }
        public string cityName { get; set; }
        public string governorateName { get; set; }
    }

}
