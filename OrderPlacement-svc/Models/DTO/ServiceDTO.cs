﻿using Moving_svc.Models.OrderPlacementDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moving_svc.Models.DTO
{
    public class ServiceDTO
    {
        public int id { get; set; }
        public string name { get; set; }
        public ServiceDTO(Service service)
        {
            this.id = service.id;
            this.name = service.name;
        }
    }
}
