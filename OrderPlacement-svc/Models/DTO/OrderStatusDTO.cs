﻿using Moving_svc.Models.OrderPlacementDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moving_svc.Models.DTO
{
    public class OrderStatusDTO
    {
        public int id { get; set; }
        public string name { get; set; }
        public OrderStatusDTO(OrderStatus orderStatus)
        {
            this.id = orderStatus.id;
            this.name = orderStatus.name;
        }
    }
}
