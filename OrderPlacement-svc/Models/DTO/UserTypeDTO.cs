﻿using Moving_svc.Models.OrderPlacementDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moving_svc.Models.DTO
{
    public class UserTypeDTO
    {
        public int id { get; set; }
        public string name { get; set; }
        public UserTypeDTO(UserType userType)
        {
            this.id = userType.id;
            this.name = userType.name;
        }
    }
}
