﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moving_svc.Models.Shared
{
    public class ResultDTO
    {
        public ResultDTO(int code)
        {
            this.code = code;
        }
        public ResultDTO(int code, dynamic result) 
        {
            this.code = code;
            this.result = result;
        }
        public ResultDTO(int code, string msg)  //Exception
        {
            this.code = code;
            this.msg = msg;
        }
        [JsonConstructor]
        public ResultDTO(int code, string msg, dynamic result)
        {
            this.code = code;
            this.msg = msg;
            this.result = result;
        }
        public dynamic result { get; set; }
        public int code { get; set; }
        public string msg { get; set; }

    }
}
