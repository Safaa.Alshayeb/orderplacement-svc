﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moving_svc.Models.Shared
{
    public class ResponseCode
    {
        public static int Ok = 200;
        public static int BadRequest = 400;
        public static int NotFound = 404;
        public static int Error = 500;
    }
}
