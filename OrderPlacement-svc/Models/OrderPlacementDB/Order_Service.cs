﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Moving_svc.Models.OrderPlacementDB
{
    public class Order_Service
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public int serviceId { get; set; }
        public virtual Service service { get; set; }
        public int orderId { get; set; }
        public virtual Order order { get; set; }
    }
}
