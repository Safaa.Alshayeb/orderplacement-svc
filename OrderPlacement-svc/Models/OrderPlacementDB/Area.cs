﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Moving_svc.Models.OrderPlacementDB
{
    public class Area
    {
            [Key]
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int id { get; set; }
            [Required(AllowEmptyStrings = false)]
            public string name { get; set; }
            public int cityId { get; set; }
            public bool isActivated { get; set; } = true;
            public DateTime creationDate { get; set; }
            public DateTime updatedDate { get; set; }
            public virtual City city { get; set; }

    }
}
