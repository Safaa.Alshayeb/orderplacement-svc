﻿using Moving_svc.Models.OrderPlacementDB;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Moving_svc.Models.OrderPlacementDB
{
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public bool isActivated { get; set; } = true;
        public DateTime creationDate { get; set; }
        public DateTime updatedDate { get; set; }
        public int customerId { get; set; }
        public virtual User customer { get; set; }
        public int orderStatusId { get; set; } = 1;
        public virtual OrderStatus orderStatus { get; set; }
        public int movingFromId { get; set; }
        public virtual Area movingFrom { get; set; }
        public string movingFromLocationDetails { get; set; }
        public int movingToId { get; set; }
        public virtual Area movingTo { get; set; }
        public string movingToLocationDetails { get; set; }
        public DateTime carriedOut { get; set; }
        public string notes { get; set; }
        public int salesConsultantId { get; set; }
        public virtual User salesConsultant { get; set; }
        public virtual ICollection<Order_Service> services { get; set; } = new HashSet<Order_Service>();

    }
}
