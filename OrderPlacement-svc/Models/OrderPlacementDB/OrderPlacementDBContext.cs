﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Moving_svc.Models.OrderPlacementDB
{
    public class OrderPlacementDBContext : DbContext
    {
        public OrderPlacementDBContext()
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json")
                   .Build();
                var connectionString = configuration["ConnectionString:OrderPlacement_Context"];
                optionsBuilder.UseMySql(connectionString, builder =>

                {
                    builder.EnableRetryOnFailure();
                });
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Service>().HasData(new Service
            {
                id = 1,
                name = "Moving",
                isActivated = true,
                creationDate = DateTime.Now,
                updatedDate = DateTime.Now

            }, new Service
            {
                id = 2,
                name = "Packing",
                isActivated = true,
                creationDate = DateTime.Now,
                updatedDate = DateTime.Now
            }, new Service
            {
                id = 3,
                name = "Cleaning",
                isActivated = true,
                creationDate = DateTime.Now,
                updatedDate = DateTime.Now
            });
            modelBuilder.Entity<UserType>().HasData(new UserType
            {
                id = 1,
                name = "Customer",
                isActivated = true,
                creationDate = DateTime.Now,
                updatedDate = DateTime.Now

            }, new Service
            {
                id = 2,
                name = "SalesConsultant",
                isActivated = true,
                creationDate = DateTime.Now,
                updatedDate = DateTime.Now
            });
            modelBuilder.Entity<OrderStatus>().HasData(new OrderStatus
            {
                id = 1,
                name = "New",
                isActivated = true,
                creationDate = DateTime.Now,
                updatedDate = DateTime.Now

            }, new OrderStatus
            {
                id = 2,
                name = "Active",
                isActivated = true,
                creationDate = DateTime.Now,
                updatedDate = DateTime.Now
            }, new OrderStatus
            {
                id = 3,
                name = "Done",
                isActivated = true,
                creationDate = DateTime.Now,
                updatedDate = DateTime.Now
            });
            modelBuilder.Entity<Governorate>().HasData(new Governorate
            {
                id = 1,
                name = "Damascuss",
                isActivated = true,
                creationDate = DateTime.Now,
                updatedDate = DateTime.Now

            }, new Governorate
            {
                id = 2,
                name = "Aleppo",
                isActivated = true,
                creationDate = DateTime.Now,
                updatedDate = DateTime.Now
            }) ;
            modelBuilder.Entity<City>().HasData(new City
            {
                id = 1,
                name = "Damascus",
                governorateId = 1,
                isActivated = true,
                creationDate = DateTime.Now,
                updatedDate = DateTime.Now

            });
            modelBuilder.Entity<Area>().HasData(new Area
            {
                id = 1,
                name = "Midan",
                isActivated = true,
                cityId = 1,
                creationDate = DateTime.Now,
                updatedDate = DateTime.Now

            }, new Area
            {
                id = 2,
                name = "Rawda",
                isActivated = true,
                cityId = 1,
                creationDate = DateTime.Now,
                updatedDate = DateTime.Now
            }
            , new Area
            {
                id = 3,
                name = "Shaalan",
                isActivated = true,
                cityId = 1,
                creationDate = DateTime.Now,
                updatedDate = DateTime.Now
            },
             new Area
             {
                 id = 4,
                 name = "Barzeh",
                 isActivated = true,
                 cityId = 1,
                 creationDate = DateTime.Now,
                 updatedDate = DateTime.Now
             },
              new Area
              {
                  id = 5,
                  name = "Salhieh",
                  isActivated = true,
                  cityId = 1,
                  creationDate = DateTime.Now,
                  updatedDate = DateTime.Now
              }
              ,
              new Area
              {
                  id = 6,
                  name = "Mazzeh",
                  isActivated = true,
                  cityId = 1,
                  creationDate = DateTime.Now,
                  updatedDate = DateTime.Now
              }
              , new Area
              {
                  id = 7,
                  name = "Adawi",
                  isActivated = true,
                  cityId = 1,
                  creationDate = DateTime.Now,
                  updatedDate = DateTime.Now
              });
              modelBuilder.Entity<User>().HasData(new User
              {
                  id = 1,
                  fullName = "SalesConsultant1",
                  isActivated = true,
                  creationDate = DateTime.Now,
                  updatedDate = DateTime.Now,
                  userTypeId = 2,
                  email = "SalesConsultant1@gmail.com",
                  phoneNumber = "0987654321"
              });
        }
        public OrderPlacementDBContext(DbContextOptions options)
            : base(options)
        {
            Database.EnsureCreated();
            Database.Migrate();
        }

        public virtual DbSet<City> City { get; set; }
        public virtual DbSet<Area> Area { get; set; }
        public virtual DbSet<Governorate> Governorate { get; set; }
        public virtual DbSet<Order> Order { get; set; }
        public virtual DbSet<Order_Service> Order_Service { get; set; }
        public virtual DbSet<OrderStatus> OrderStatus { get; set; }
        public virtual DbSet<Service> Service { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserType> UserType { get; set; }
    }
}
