﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Moving_svc.Models.OrderPlacementDB
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string fullName { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string phoneNumber { get; set; }
        public bool isActivated { get; set; } = true;
        public DateTime creationDate { get; set; }
        public DateTime updatedDate { get; set; }
        [Required(ErrorMessage = "The Email field is required.")]
        [EmailAddress(ErrorMessage = "The Email field is not a valid e-mail address.")]
        public string email { get; set; }
        public int userTypeId { get; set; }
        public virtual UserType userType { get; set; }
    }
}
