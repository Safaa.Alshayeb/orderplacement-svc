﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Moving_svc.Models.OrderPlacementDB;

namespace Moving_svc.Migrations
{
    [DbContext(typeof(OrderPlacementDBContext))]
    partial class OrderPlacementDBContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.6")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("Moving_svc.Models.OrderPlacementDB.Area", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<int>("cityId")
                        .HasColumnType("int");

                    b.Property<DateTime>("creationDate")
                        .HasColumnType("datetime(6)");

                    b.Property<bool>("isActivated")
                        .HasColumnType("tinyint(1)");

                    b.Property<string>("name")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<DateTime>("updatedDate")
                        .HasColumnType("datetime(6)");

                    b.HasKey("id");

                    b.HasIndex("cityId");

                    b.ToTable("Area");

                    b.HasData(
                        new
                        {
                            id = 1,
                            cityId = 1,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(783),
                            isActivated = true,
                            name = "Midan",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(1353)
                        },
                        new
                        {
                            id = 2,
                            cityId = 1,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2033),
                            isActivated = true,
                            name = "Rawda",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2056)
                        },
                        new
                        {
                            id = 3,
                            cityId = 1,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2068),
                            isActivated = true,
                            name = "Shaalan",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2072)
                        },
                        new
                        {
                            id = 4,
                            cityId = 1,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2077),
                            isActivated = true,
                            name = "Barzeh",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2080)
                        },
                        new
                        {
                            id = 5,
                            cityId = 1,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2087),
                            isActivated = true,
                            name = "Salhieh",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2090)
                        },
                        new
                        {
                            id = 6,
                            cityId = 1,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2094),
                            isActivated = true,
                            name = "Mazzeh",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2098)
                        },
                        new
                        {
                            id = 7,
                            cityId = 1,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2102),
                            isActivated = true,
                            name = "Adawi",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2106)
                        });
                });

            modelBuilder.Entity("Moving_svc.Models.OrderPlacementDB.City", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<DateTime>("creationDate")
                        .HasColumnType("datetime(6)");

                    b.Property<int>("governorateId")
                        .HasColumnType("int");

                    b.Property<bool>("isActivated")
                        .HasColumnType("tinyint(1)");

                    b.Property<string>("name")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<DateTime>("updatedDate")
                        .HasColumnType("datetime(6)");

                    b.HasKey("id");

                    b.HasIndex("governorateId");

                    b.ToTable("City");

                    b.HasData(
                        new
                        {
                            id = 1,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 490, DateTimeKind.Local).AddTicks(6868),
                            governorateId = 1,
                            isActivated = true,
                            name = "Damascus",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 490, DateTimeKind.Local).AddTicks(7412)
                        });
                });

            modelBuilder.Entity("Moving_svc.Models.OrderPlacementDB.Governorate", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<DateTime>("creationDate")
                        .HasColumnType("datetime(6)");

                    b.Property<bool>("isActivated")
                        .HasColumnType("tinyint(1)");

                    b.Property<string>("name")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<DateTime>("updatedDate")
                        .HasColumnType("datetime(6)");

                    b.HasKey("id");

                    b.ToTable("Governorate");

                    b.HasData(
                        new
                        {
                            id = 1,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 490, DateTimeKind.Local).AddTicks(1368),
                            isActivated = true,
                            name = "Damascuss",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 490, DateTimeKind.Local).AddTicks(1949)
                        },
                        new
                        {
                            id = 2,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 490, DateTimeKind.Local).AddTicks(2545),
                            isActivated = true,
                            name = "Aleppo",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 490, DateTimeKind.Local).AddTicks(2569)
                        });
                });

            modelBuilder.Entity("Moving_svc.Models.OrderPlacementDB.Order", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<DateTime>("carriedOut")
                        .HasColumnType("datetime(6)");

                    b.Property<DateTime>("creationDate")
                        .HasColumnType("datetime(6)");

                    b.Property<int>("customerId")
                        .HasColumnType("int");

                    b.Property<bool>("isActivated")
                        .HasColumnType("tinyint(1)");

                    b.Property<int>("movingFromId")
                        .HasColumnType("int");

                    b.Property<string>("movingFromLocationDetails")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<int>("movingToId")
                        .HasColumnType("int");

                    b.Property<string>("movingToLocationDetails")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("notes")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<int>("orderStatusId")
                        .HasColumnType("int");

                    b.Property<int>("salesConsultantId")
                        .HasColumnType("int");

                    b.Property<DateTime>("updatedDate")
                        .HasColumnType("datetime(6)");

                    b.HasKey("id");

                    b.HasIndex("customerId");

                    b.HasIndex("movingFromId");

                    b.HasIndex("movingToId");

                    b.HasIndex("orderStatusId");

                    b.HasIndex("salesConsultantId");

                    b.ToTable("Order");
                });

            modelBuilder.Entity("Moving_svc.Models.OrderPlacementDB.OrderStatus", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<DateTime>("creationDate")
                        .HasColumnType("datetime(6)");

                    b.Property<bool>("isActivated")
                        .HasColumnType("tinyint(1)");

                    b.Property<string>("name")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<DateTime>("updatedDate")
                        .HasColumnType("datetime(6)");

                    b.HasKey("id");

                    b.ToTable("OrderStatus");

                    b.HasData(
                        new
                        {
                            id = 1,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(5529),
                            isActivated = true,
                            name = "New",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(6084)
                        },
                        new
                        {
                            id = 2,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(6726),
                            isActivated = true,
                            name = "Active",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(6750)
                        },
                        new
                        {
                            id = 3,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(6769),
                            isActivated = true,
                            name = "Done",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(6773)
                        });
                });

            modelBuilder.Entity("Moving_svc.Models.OrderPlacementDB.Order_Service", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<int>("orderId")
                        .HasColumnType("int");

                    b.Property<int>("serviceId")
                        .HasColumnType("int");

                    b.HasKey("id");

                    b.HasIndex("orderId");

                    b.HasIndex("serviceId");

                    b.ToTable("Order_Service");
                });

            modelBuilder.Entity("Moving_svc.Models.OrderPlacementDB.Service", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<DateTime>("creationDate")
                        .HasColumnType("datetime(6)");

                    b.Property<bool>("isActivated")
                        .HasColumnType("tinyint(1)");

                    b.Property<string>("name")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<DateTime>("updatedDate")
                        .HasColumnType("datetime(6)");

                    b.HasKey("id");

                    b.ToTable("Service");

                    b.HasData(
                        new
                        {
                            id = 1,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 483, DateTimeKind.Local).AddTicks(5631),
                            isActivated = true,
                            name = "Moving",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 486, DateTimeKind.Local).AddTicks(7651)
                        },
                        new
                        {
                            id = 2,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 486, DateTimeKind.Local).AddTicks(8388),
                            isActivated = true,
                            name = "Packing",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 486, DateTimeKind.Local).AddTicks(8412)
                        },
                        new
                        {
                            id = 3,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 486, DateTimeKind.Local).AddTicks(8424),
                            isActivated = true,
                            name = "Cleaning",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 486, DateTimeKind.Local).AddTicks(8428)
                        });
                });

            modelBuilder.Entity("Moving_svc.Models.OrderPlacementDB.User", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<DateTime>("creationDate")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("email")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("fullName")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<bool>("isActivated")
                        .HasColumnType("tinyint(1)");

                    b.Property<string>("phoneNumber")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<DateTime>("updatedDate")
                        .HasColumnType("datetime(6)");

                    b.Property<int>("userTypeId")
                        .HasColumnType("int");

                    b.HasKey("id");

                    b.HasIndex("userTypeId");

                    b.ToTable("User");

                    b.HasData(
                        new
                        {
                            id = 1,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(4556),
                            email = "SalesConsultant1@gmail.com",
                            fullName = "SalesConsultant1",
                            isActivated = true,
                            phoneNumber = "0987654321",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(5089),
                            userTypeId = 2
                        });
                });

            modelBuilder.Entity("Moving_svc.Models.OrderPlacementDB.UserType", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<DateTime>("creationDate")
                        .HasColumnType("datetime(6)");

                    b.Property<bool>("isActivated")
                        .HasColumnType("tinyint(1)");

                    b.Property<string>("name")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<DateTime>("updatedDate")
                        .HasColumnType("datetime(6)");

                    b.HasKey("id");

                    b.ToTable("UserType");

                    b.HasData(
                        new
                        {
                            id = 1,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(633),
                            isActivated = true,
                            name = "Customer",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(1527)
                        },
                        new
                        {
                            id = 2,
                            creationDate = new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(2092),
                            isActivated = true,
                            name = "SalesConsultant",
                            updatedDate = new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(2109)
                        });
                });

            modelBuilder.Entity("Moving_svc.Models.OrderPlacementDB.Area", b =>
                {
                    b.HasOne("Moving_svc.Models.OrderPlacementDB.City", "city")
                        .WithMany("areas")
                        .HasForeignKey("cityId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Moving_svc.Models.OrderPlacementDB.City", b =>
                {
                    b.HasOne("Moving_svc.Models.OrderPlacementDB.Governorate", "governorate")
                        .WithMany("Cities")
                        .HasForeignKey("governorateId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Moving_svc.Models.OrderPlacementDB.Order", b =>
                {
                    b.HasOne("Moving_svc.Models.OrderPlacementDB.User", "customer")
                        .WithMany()
                        .HasForeignKey("customerId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Moving_svc.Models.OrderPlacementDB.Area", "movingFrom")
                        .WithMany()
                        .HasForeignKey("movingFromId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Moving_svc.Models.OrderPlacementDB.Area", "movingTo")
                        .WithMany()
                        .HasForeignKey("movingToId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Moving_svc.Models.OrderPlacementDB.OrderStatus", "orderStatus")
                        .WithMany()
                        .HasForeignKey("orderStatusId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Moving_svc.Models.OrderPlacementDB.User", "salesConsultant")
                        .WithMany()
                        .HasForeignKey("salesConsultantId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Moving_svc.Models.OrderPlacementDB.Order_Service", b =>
                {
                    b.HasOne("Moving_svc.Models.OrderPlacementDB.Order", "order")
                        .WithMany("services")
                        .HasForeignKey("orderId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Moving_svc.Models.OrderPlacementDB.Service", "service")
                        .WithMany()
                        .HasForeignKey("serviceId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Moving_svc.Models.OrderPlacementDB.User", b =>
                {
                    b.HasOne("Moving_svc.Models.OrderPlacementDB.UserType", "userType")
                        .WithMany()
                        .HasForeignKey("userTypeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
