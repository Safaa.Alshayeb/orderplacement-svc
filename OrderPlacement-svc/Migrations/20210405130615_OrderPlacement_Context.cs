﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Moving_svc.Migrations
{
    public partial class OrderPlacement_Context : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Governorate",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(nullable: false),
                    isActivated = table.Column<bool>(nullable: false),
                    creationDate = table.Column<DateTime>(nullable: false),
                    updatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Governorate", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "OrderStatus",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(nullable: false),
                    isActivated = table.Column<bool>(nullable: false),
                    creationDate = table.Column<DateTime>(nullable: false),
                    updatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderStatus", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Service",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(nullable: false),
                    isActivated = table.Column<bool>(nullable: false),
                    creationDate = table.Column<DateTime>(nullable: false),
                    updatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Service", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "UserType",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(nullable: false),
                    isActivated = table.Column<bool>(nullable: false),
                    creationDate = table.Column<DateTime>(nullable: false),
                    updatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserType", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "City",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(nullable: false),
                    governorateId = table.Column<int>(nullable: false),
                    isActivated = table.Column<bool>(nullable: false),
                    creationDate = table.Column<DateTime>(nullable: false),
                    updatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_City", x => x.id);
                    table.ForeignKey(
                        name: "FK_City_Governorate_governorateId",
                        column: x => x.governorateId,
                        principalTable: "Governorate",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    fullName = table.Column<string>(nullable: false),
                    phoneNumber = table.Column<string>(nullable: false),
                    isActivated = table.Column<bool>(nullable: false),
                    creationDate = table.Column<DateTime>(nullable: false),
                    updatedDate = table.Column<DateTime>(nullable: false),
                    email = table.Column<string>(nullable: false),
                    userTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.id);
                    table.ForeignKey(
                        name: "FK_User_UserType_userTypeId",
                        column: x => x.userTypeId,
                        principalTable: "UserType",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Area",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(nullable: false),
                    cityId = table.Column<int>(nullable: false),
                    isActivated = table.Column<bool>(nullable: false),
                    creationDate = table.Column<DateTime>(nullable: false),
                    updatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Area", x => x.id);
                    table.ForeignKey(
                        name: "FK_Area_City_cityId",
                        column: x => x.cityId,
                        principalTable: "City",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Order",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    isActivated = table.Column<bool>(nullable: false),
                    creationDate = table.Column<DateTime>(nullable: false),
                    updatedDate = table.Column<DateTime>(nullable: false),
                    customerId = table.Column<int>(nullable: false),
                    orderStatusId = table.Column<int>(nullable: false),
                    movingFromId = table.Column<int>(nullable: false),
                    movingFromLocationDetails = table.Column<string>(nullable: true),
                    movingToId = table.Column<int>(nullable: false),
                    movingToLocationDetails = table.Column<string>(nullable: true),
                    carriedOut = table.Column<DateTime>(nullable: false),
                    notes = table.Column<string>(nullable: true),
                    salesConsultantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order", x => x.id);
                    table.ForeignKey(
                        name: "FK_Order_User_customerId",
                        column: x => x.customerId,
                        principalTable: "User",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Order_Area_movingFromId",
                        column: x => x.movingFromId,
                        principalTable: "Area",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Order_Area_movingToId",
                        column: x => x.movingToId,
                        principalTable: "Area",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Order_OrderStatus_orderStatusId",
                        column: x => x.orderStatusId,
                        principalTable: "OrderStatus",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Order_User_salesConsultantId",
                        column: x => x.salesConsultantId,
                        principalTable: "User",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Order_Service",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    serviceId = table.Column<int>(nullable: false),
                    orderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order_Service", x => x.id);
                    table.ForeignKey(
                        name: "FK_Order_Service_Order_orderId",
                        column: x => x.orderId,
                        principalTable: "Order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Order_Service_Service_serviceId",
                        column: x => x.serviceId,
                        principalTable: "Service",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Governorate",
                columns: new[] { "id", "creationDate", "isActivated", "name", "updatedDate" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 4, 5, 16, 6, 13, 490, DateTimeKind.Local).AddTicks(1368), true, "Damascuss", new DateTime(2021, 4, 5, 16, 6, 13, 490, DateTimeKind.Local).AddTicks(1949) },
                    { 2, new DateTime(2021, 4, 5, 16, 6, 13, 490, DateTimeKind.Local).AddTicks(2545), true, "Aleppo", new DateTime(2021, 4, 5, 16, 6, 13, 490, DateTimeKind.Local).AddTicks(2569) }
                });

            migrationBuilder.InsertData(
                table: "OrderStatus",
                columns: new[] { "id", "creationDate", "isActivated", "name", "updatedDate" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(5529), true, "New", new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(6084) },
                    { 2, new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(6726), true, "Active", new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(6750) },
                    { 3, new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(6769), true, "Done", new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(6773) }
                });

            migrationBuilder.InsertData(
                table: "Service",
                columns: new[] { "id", "creationDate", "isActivated", "name", "updatedDate" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 4, 5, 16, 6, 13, 483, DateTimeKind.Local).AddTicks(5631), true, "Moving", new DateTime(2021, 4, 5, 16, 6, 13, 486, DateTimeKind.Local).AddTicks(7651) },
                    { 2, new DateTime(2021, 4, 5, 16, 6, 13, 486, DateTimeKind.Local).AddTicks(8388), true, "Packing", new DateTime(2021, 4, 5, 16, 6, 13, 486, DateTimeKind.Local).AddTicks(8412) },
                    { 3, new DateTime(2021, 4, 5, 16, 6, 13, 486, DateTimeKind.Local).AddTicks(8424), true, "Cleaning", new DateTime(2021, 4, 5, 16, 6, 13, 486, DateTimeKind.Local).AddTicks(8428) }
                });

            migrationBuilder.InsertData(
                table: "UserType",
                columns: new[] { "id", "creationDate", "isActivated", "name", "updatedDate" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(633), true, "Customer", new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(1527) },
                    { 2, new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(2092), true, "SalesConsultant", new DateTime(2021, 4, 5, 16, 6, 13, 489, DateTimeKind.Local).AddTicks(2109) }
                });

            migrationBuilder.InsertData(
                table: "City",
                columns: new[] { "id", "creationDate", "governorateId", "isActivated", "name", "updatedDate" },
                values: new object[] { 1, new DateTime(2021, 4, 5, 16, 6, 13, 490, DateTimeKind.Local).AddTicks(6868), 1, true, "Damascus", new DateTime(2021, 4, 5, 16, 6, 13, 490, DateTimeKind.Local).AddTicks(7412) });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "id", "creationDate", "email", "fullName", "isActivated", "phoneNumber", "updatedDate", "userTypeId" },
                values: new object[] { 1, new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(4556), "SalesConsultant1@gmail.com", "SalesConsultant1", true, "0987654321", new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(5089), 2 });

            migrationBuilder.InsertData(
                table: "Area",
                columns: new[] { "id", "cityId", "creationDate", "isActivated", "name", "updatedDate" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(783), true, "Midan", new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(1353) },
                    { 2, 1, new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2033), true, "Rawda", new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2056) },
                    { 3, 1, new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2068), true, "Shaalan", new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2072) },
                    { 4, 1, new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2077), true, "Barzeh", new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2080) },
                    { 5, 1, new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2087), true, "Salhieh", new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2090) },
                    { 6, 1, new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2094), true, "Mazzeh", new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2098) },
                    { 7, 1, new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2102), true, "Adawi", new DateTime(2021, 4, 5, 16, 6, 13, 491, DateTimeKind.Local).AddTicks(2106) }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Area_cityId",
                table: "Area",
                column: "cityId");

            migrationBuilder.CreateIndex(
                name: "IX_City_governorateId",
                table: "City",
                column: "governorateId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_customerId",
                table: "Order",
                column: "customerId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_movingFromId",
                table: "Order",
                column: "movingFromId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_movingToId",
                table: "Order",
                column: "movingToId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_orderStatusId",
                table: "Order",
                column: "orderStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_salesConsultantId",
                table: "Order",
                column: "salesConsultantId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_Service_orderId",
                table: "Order_Service",
                column: "orderId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_Service_serviceId",
                table: "Order_Service",
                column: "serviceId");

            migrationBuilder.CreateIndex(
                name: "IX_User_userTypeId",
                table: "User",
                column: "userTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Order_Service");

            migrationBuilder.DropTable(
                name: "Order");

            migrationBuilder.DropTable(
                name: "Service");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Area");

            migrationBuilder.DropTable(
                name: "OrderStatus");

            migrationBuilder.DropTable(
                name: "UserType");

            migrationBuilder.DropTable(
                name: "City");

            migrationBuilder.DropTable(
                name: "Governorate");
        }
    }
}
