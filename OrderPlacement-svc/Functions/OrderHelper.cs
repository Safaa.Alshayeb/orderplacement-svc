﻿using Microsoft.EntityFrameworkCore;
using Moving_svc.Models.DTO;
using Moving_svc.Models.Input;
using Moving_svc.Models.OrderPlacementDB;
using Moving_svc.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moving_svc.Functions
{
    public class OrderHelper
    {
        private OrderPlacementDBContext _context;
        public OrderHelper()
        {
            _context = new OrderPlacementDBContext();
        }
        public ResultDTO GetOrder(int id)
        {
            try
            {
                if (OrderExists(id) == false)
                    return new ResultDTO(ResponseCode.NotFound, "Order id isn't found");
                var order = _context.Order.Include(d => d.customer).ThenInclude(e=> e.userType)
                    .Include(d => d.services).ThenInclude(a => a.service)
                    .Include(d=> d.orderStatus).Include(d=>d.salesConsultant).ThenInclude(e => e.userType)
                    .Include(d=> d.movingTo).ThenInclude(s=> s.city).ThenInclude(x=> x.governorate)
                    .Include(d => d.movingFrom).ThenInclude(s => s.city).ThenInclude(x => x.governorate)
                    .Where(s => s.id == id).FirstOrDefault();
                return new ResultDTO(ResponseCode.Ok, new OrderDTO(order));
            }
            catch (Exception ex)
            {
                return new ResultDTO(ResponseCode.Error, ex.ToString());
            }
        }
        public ResultDTO GetOrdersByCustomer(int customerId)
        {
            try
            {
                CustomerHelper customerHelper = new CustomerHelper();
                if (customerHelper.CustomerExists(customerId) == false)
                    return new ResultDTO(ResponseCode.NotFound, "Customer id isn't found");
                var orders = _context.Order.Include(d => d.customer).ThenInclude(e => e.userType)
                    .Include(d => d.services).ThenInclude(a => a.service)
                    .Include(d => d.orderStatus).Include(d => d.salesConsultant).ThenInclude(e => e.userType)
                    .Include(d => d.movingTo).ThenInclude(s => s.city).ThenInclude(x => x.governorate)
                    .Include(d => d.movingFrom).ThenInclude(s => s.city).ThenInclude(x => x.governorate)
                    .Where(s => s.customerId == customerId).Select(x => new OrderDTO(x)).ToList();
                return new ResultDTO(ResponseCode.Ok, orders);
            }
            catch (Exception ex)
            {
                return new ResultDTO(ResponseCode.Error, ex.ToString());
            }
        }
        public ResultDTO GetOrdersByPhoneNumber(string phoneNumber)
        {
            try
            {
                CustomerHelper customerHelper = new CustomerHelper();
                var customer = customerHelper.GetCustomerByphoneNumber(phoneNumber);
                if (customer != null)
                {
                    var orders = _context.Order.Include(d => d.customer).ThenInclude(e => e.userType)
                    .Include(d => d.services).ThenInclude(a=>a.service)
                    .Include(d => d.orderStatus).Include(d => d.salesConsultant).ThenInclude(e => e.userType)
                    .Include(d => d.movingTo).ThenInclude(s => s.city).ThenInclude(x => x.governorate)
                    .Include(d => d.movingFrom).ThenInclude(s => s.city).ThenInclude(x => x.governorate)
                    .Where(s => s.customerId == customer.id).Select(x => new OrderDTO(x)).ToList();
                    return new ResultDTO(ResponseCode.Ok, orders);
                }
                else
                    return new ResultDTO(ResponseCode.NotFound, "Customer phone number isn't found");
            }
            catch (Exception ex)
            {
                return new ResultDTO(ResponseCode.Error, ex.ToString());
            }
        }
        public ResultDTO PlaceOrder(OrderInput orderInput)
        {
   
            try
            {
                Order order = new Order();
                var strategy = _context.Database.CreateExecutionStrategy();
                strategy.Execute(
                () =>
                {
                    var tran = _context.Database.BeginTransaction();
                    CustomerHelper customerHelper = new CustomerHelper();
                    var customer = customerHelper.GetCustomerByphoneNumber(orderInput.customerInput.phoneNumber);
                    if (customer != null)
                    {
                        order.customerId = customer.id;
                    }
                    else
                    {
                        var newcustomer = customerHelper.AddCustomer(orderInput.customerInput);
                        order.customerId = newcustomer.id;
                    }
                    order.orderStatusId = 1;//new
                    order.movingFromId = orderInput.movingFromId;
                    order.movingFromLocationDetails = orderInput.movingFromLocationDetails;
                    order.movingToId = orderInput.movingToId;
                    order.movingToLocationDetails = orderInput.movingToLocationDetails;
                    order.movingFromId = orderInput.movingFromId;
                    order.carriedOut = orderInput.carriedOut;
                    order.notes = orderInput.notes;
                    order.salesConsultantId = orderInput.salesConsultantId;
                    order.creationDate = DateTime.Now;
                    order.updatedDate = DateTime.Now;
                    _context.Order.Add(order);
                    _context.SaveChanges();
                    foreach (int s in orderInput.services)
                    {
                        Order_Service order_Service = new Order_Service();
                        order_Service.serviceId = s;
                        order_Service.orderId = order.id;
                        _context.Order_Service.Add(order_Service);
                        _context.SaveChanges();
                    }
                    tran.Commit();
                });
                return GetOrder(order.id);
            }
            catch (Exception ex)
            {
                return new ResultDTO(ResponseCode.Error, ex.ToString());
            }
        }
        public ResultDTO EditOrder(int id, OrderInput orderInput)
        {
            try
            {
                if (OrderExists(id) == false)
                    return new ResultDTO(ResponseCode.NotFound, "Order id isn't found");
                Order order = _context.Order.Include(d => d.services).ThenInclude(a => a.service).FirstOrDefault(d => d.id == id);
                var strategy = _context.Database.CreateExecutionStrategy();
                strategy.Execute(
                () =>
                {
                    var tran = _context.Database.BeginTransaction();
                    order.updatedDate = DateTime.Now;
                    if(orderInput.orderStatusId != null)
                       order.orderStatusId = (int)orderInput.orderStatusId;
                    order.movingFromId = orderInput.movingFromId;
                    order.movingFromLocationDetails = orderInput.movingFromLocationDetails;
                    order.movingToId = orderInput.movingToId;
                    order.movingToLocationDetails = orderInput.movingToLocationDetails;
                    order.movingFromId = orderInput.movingFromId;
                    order.carriedOut = orderInput.carriedOut;
                    order.notes = orderInput.notes;
                    order.salesConsultantId = orderInput.salesConsultantId;
                    _context.Order_Service.RemoveRange(order.services);
                    _context.SaveChanges();
                    foreach (int s in orderInput.services)
                    {
                        Order_Service order_Service = new Order_Service();
                        order_Service.serviceId = s;
                        order_Service.orderId = order.id;
                        _context.Order_Service.Add(order_Service);
                        _context.SaveChanges();
                    }
                    CustomerHelper customerHelper = new CustomerHelper();
                    customerHelper.EditCustomer(order.customerId, orderInput.customerInput);
                    _context.Entry(order).State = EntityState.Modified;
                    _context.SaveChanges();
                    tran.Commit();
                });

                
                return GetOrder(order.id);
            }
            catch (Exception ex)
            {
                return new ResultDTO(ResponseCode.Error, ex.ToString());
            }
        }
        public ResultDTO DeleteOrder(int id)
        {
            try
            {
                if (OrderExists(id) == false)
                    return new ResultDTO(ResponseCode.NotFound, "Order id isn't found");
                Order order = _context.Order.Include(d=>d.services).FirstOrDefault(d => d.id == id);
                _context.Order_Service.RemoveRange(order.services);
                _context.SaveChanges();
                _context.Order.Remove(order);
                _context.SaveChanges();
                return new ResultDTO(ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                return new ResultDTO(ResponseCode.Error, ex.ToString());
            }
        }
        private bool OrderExists(int id)
        {
            return _context.Order.Any(e => e.id == id);
        }
    }
}
