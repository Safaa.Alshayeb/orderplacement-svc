﻿using Microsoft.EntityFrameworkCore;
using Moving_svc.Models.DTO;
using Moving_svc.Models.Input;
using Moving_svc.Models.OrderPlacementDB;
using Moving_svc.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moving_svc.Functions
{
    public class CustomerHelper
    {
        private OrderPlacementDBContext _context;
        public CustomerHelper()
        {
            _context = new OrderPlacementDBContext();

        }
        public UserDTO GetCustomerByphoneNumber(string phoneNumber)
        {
            try
            {
                var customer = _context.User.Include(d => d.userType)
                    .Where(s => s.phoneNumber == phoneNumber).FirstOrDefault();
                if (customer != null)
                    return new UserDTO(customer);
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public UserDTO AddCustomer(CustomerInput customerInput)
        {

            try
            {
                User customer = new User();
                customer.fullName = customerInput.fullName;
                customer.email = customerInput.email;
                customer.phoneNumber = customerInput.phoneNumber;
                customer.userTypeId = 1;//customer
                customer.isActivated = true;
                customer.creationDate = DateTime.Now;
                customer.updatedDate = DateTime.Now;
                _context.User.Add(customer);
                _context.SaveChanges();
                return GetCustomer(customer.id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public UserDTO GetCustomer(int id)
        {
            try
            {
                if (CustomerExists(id) == false)
                    return null;
                var customer = _context.User.Include(d => d.userType)
                   .FirstOrDefault(d=> d.id == id);
                return new UserDTO(customer);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public UserDTO EditCustomer(int id, CustomerInput customerInput)
        {
            try
            {
                if (CustomerExists(id) == false)
                    return null;
                User customer = _context.User.FirstOrDefault(d => d.id == id);
                customer.updatedDate = DateTime.Now;
                customer.fullName = customerInput.fullName;
                customer.email = customerInput.email;
                customer.phoneNumber = customerInput.phoneNumber;
                _context.Entry(customer).State = EntityState.Modified;
                _context.SaveChanges();
                return GetCustomer(id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool CustomerExists(int id)
        {
            return _context.User.Any(e => e.id == id && e.userTypeId == 1);
        }
    }
}
