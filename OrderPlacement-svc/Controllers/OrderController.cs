﻿using Microsoft.AspNetCore.Mvc;
using Moving_svc.Functions;
using Moving_svc.Models.Input;
using Moving_svc.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moving_svc.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        
        [HttpGet("{id}")]
        public ResultDTO GetOrder(int id)
        {
            OrderHelper orderHelper = new OrderHelper();
            return orderHelper.GetOrder(id);
        }
        [HttpGet("GetOrdersByCustomer/{customerId}")]
        public ResultDTO GetOrdersByCustomer(int customerId)
        {
            OrderHelper orderHelper = new OrderHelper();
            return orderHelper.GetOrdersByCustomer(customerId);
        }
        [HttpGet("GetOrdersByPhoneNumber/{PhoneNumber}")]
        public ResultDTO GetOrdersByPhoneNumber(string PhoneNumber)
        {
            OrderHelper orderHelper = new OrderHelper();
            return orderHelper.GetOrdersByPhoneNumber(PhoneNumber);
        }
        [HttpPost]
        public ResultDTO PlaceOrder([FromForm] OrderInput orderInput)
        {
            OrderHelper orderHelper = new OrderHelper();
            return orderHelper.PlaceOrder(orderInput);
        }
        [HttpPut]
        public ResultDTO EditOrder(int id, [FromForm] OrderInput orderInput)
        {
            OrderHelper orderHelper = new OrderHelper();
            return orderHelper.EditOrder(id, orderInput);
        }
        [HttpDelete("{id}")]
        public ResultDTO DeleteOrder(int id)
        {
            OrderHelper orderHelper = new OrderHelper();
            return orderHelper.DeleteOrder(id);
        }
    }
}
